import Vue from 'vue'
//import Patron from './Patron.vue'
//import Books from './Books.vue'
import Dashboard from './Dashboard.vue'
//import Settings from './Settings.vue'
import './assets/css/style.css'

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.config.productionTip = false
new Vue({
  render: function (h) { return h(Dashboard) },
  //render: function (h) { return h(Patron) },
  //render: function (h) { return h(Books) },
  //render: function (h) { return h(Settings) },
}).$mount('#app')
